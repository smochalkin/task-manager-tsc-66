package ru.smochalkin.tm.api.model;

import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasEndDate {

    @Nullable
    Date getEndDate();

    void setEndDate(@Nullable final Date name);

}
