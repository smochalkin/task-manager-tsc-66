package ru.smochalkin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.smochalkin.tm.dto.ProjectDto;

import java.util.List;

public interface ProjectRepository extends JpaRepository<ProjectDto, String> {

    void deleteByUserId(@NotNull String userId);

    @NotNull
    List<ProjectDto> findAllByUserId(@NotNull String userId);

    @Nullable
    ProjectDto findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    ProjectDto findFirstByUserIdAndName(@NotNull String userId, @NotNull String name);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndName(@NotNull String userId, @NotNull String name);

    long countByUserId(@NotNull String userId);

}
