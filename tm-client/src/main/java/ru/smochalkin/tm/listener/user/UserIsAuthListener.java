package ru.smochalkin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.smochalkin.tm.event.ConsoleEvent;
import ru.smochalkin.tm.listener.AbstractListener;

@Component
public class UserIsAuthListener extends AbstractListener {

    @Override
    @NotNull
    public String name() {
        return "user-auth";
    }

    @Override
    @NotNull
    public String description() {
        return "Check user auth.";
    }

    @Override
    @EventListener(condition = "@userIsAuthListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("User auth: " + (sessionService.getSession() != null));
    }

}
