package ru.smochalkin.tm;

import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.smochalkin.tm.api.IProjectRestEndpoint;
import ru.smochalkin.tm.client.ProjectFeignClient;
import ru.smochalkin.tm.marker.RestCategory;
import ru.smochalkin.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectEndpointTest {

    final Project project1 = new Project("test project 1");

    final Project project2 = new Project("test project 2");

    final IProjectRestEndpoint client = ProjectFeignClient.client();

    @Before
    public void before() {
        client.create(project1);
    }

    @After
    @SneakyThrows
    public void after() {
        client.deleteAll();
    }

    @Test
    @Category(RestCategory.class)
    public void find() {
        client.find(
                project1.getId()
        );

        Assert.assertEquals(
                project1.getName(),
                client.find(
                        project1.getId()
                        )
                        .getName());
    }

    @Test
    @Category(RestCategory.class)
    public void create() {
        Assert.assertNotNull(client.create(project2));
    }

    @Test
    @Category(RestCategory.class)
    public void update() {
        final Project updatedProject = client.find(project1.getId());
        updatedProject.setName("updated");
        client.save(updatedProject);
        Assert.assertEquals("updated", client.find(project1.getId()).getName());
    }

    @Test
    @Category(RestCategory.class)
    public void delete() {
        client.delete(project1.getId());
        Assert.assertNull(client.find(project1.getId()));
    }

    @Test
    @Category(RestCategory.class)
    public void findAll() {
        Assert.assertEquals(1, client.findAll().size());
        client.create(project2);
        Assert.assertEquals(2, client.findAll().size());
    }

    @Test
    @Category(RestCategory.class)
    public void updateAll() {
        client.create(project2);
        final Project updatedProject = client.find(project1.getId());
        updatedProject.setName("updated1");
        final Project updatedProject2 = client.find(project2.getId());
        updatedProject2.setName("updated2");
        List<Project> updatedList = new ArrayList<>();
        updatedList.add(updatedProject);
        updatedList.add(updatedProject2);
        client.saveAll(updatedList);
        Assert.assertEquals("updated1", client.find(project1.getId()).getName());
        Assert.assertEquals("updated2", client.find(project2.getId()).getName());
    }

    @Test
    @Category(RestCategory.class)
    public void createAll() {
        client.delete(project1.getId());
        List<Project> list = new ArrayList<>();
        list.add(project1);
        list.add(project2);
        client.createAll(list);
        Assert.assertEquals("test project 1", client.find(project1.getId()).getName());
        Assert.assertEquals("test project 2", client.find(project2.getId()).getName());
    }

    @Test
    @Category(RestCategory.class)
    public void deleteAll() {
        client.create(project2);
        Assert.assertEquals(2, client.findAll().size());
        client.deleteAll();
        Assert.assertEquals(0, client.findAll().size());
    }

}
