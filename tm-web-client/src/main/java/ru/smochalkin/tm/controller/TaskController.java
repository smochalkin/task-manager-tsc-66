package ru.smochalkin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.api.service.ITaskService;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.model.Task;

import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @GetMapping("/task/create")
    public String create() {
        taskService.create();
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        taskService.removeById(id);
        return "redirect:/tasks";
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        return new ModelAndView(
                "task-edit",
                "task", taskService.findById(id)
        );
    }

    @PostMapping("/task/edit")
    public String edit(@ModelAttribute("task") Task task) {
        taskService.save(task);
        return "redirect:/tasks";
    }

    @ModelAttribute("viewName")
    public String getViewName() {
        return "Task edit";
    }

    @ModelAttribute("projects")
    public List<Project> getProjects() {
        return projectService.findAll();
    }

}