package ru.smochalkin.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.smochalkin.tm.model.Project;

public interface IProjectRepository extends JpaRepository<Project, String> {
}
