package ru.smochalkin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.smochalkin.tm.api.IProjectRestEndpoint;
import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return new ArrayList<>(projectService.findAll());
    }

    @Override
    @GetMapping("/find/{id}")
    public Project find(@PathVariable("id") final String id) {
        return projectService.findById(id);
    }

    @Override
    @PostMapping("/create")
    public Project create(@RequestBody final Project project) {
        projectService.save(project);
        return project;
    }

    @Override
    @PostMapping("/createAll")
    public List<Project> createAll(@RequestBody final List<Project> projects) {
        projectService.addAll(projects);
        return projects;
    }

    @Override
    @PutMapping("/save")
    public Project save(@RequestBody final Project project) {
        projectService.save(project);
        return project;
    }

    @Override
    @PutMapping("/saveAll")
    public List<Project> saveAll(@RequestBody final List<Project> projects) {
        projectService.addAll(projects);
        return projects;
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") final String id) {
        projectService.removeById(id);
    }

    @Override
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        projectService.clear();
    }

}