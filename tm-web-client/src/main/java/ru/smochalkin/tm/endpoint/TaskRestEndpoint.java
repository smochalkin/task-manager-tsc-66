package ru.smochalkin.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.smochalkin.tm.api.ITaskRestEndpoint;
import ru.smochalkin.tm.api.service.ITaskService;
import ru.smochalkin.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return new ArrayList<>(taskService.findAll());
    }

    @Override
    @GetMapping("/find/{id}")
    public Task find(@PathVariable("id") final String id) {
        return taskService.findById(id);
    }

    @Override
    @PostMapping("/create")
    public Task create(@RequestBody final Task task) {
        taskService.save(task);
        return task;
    }

    @Override
    @PostMapping("/createAll")
    public List<Task> createAll(@RequestBody final List<Task> tasks) {
        taskService.addAll(tasks);
        return tasks;
    }

    @Override
    @PutMapping("/save")
    public Task save(@RequestBody final Task task) {
        taskService.save(task);
        return task;
    }

    @Override
    @PutMapping("/saveAll")
    public List<Task> saveAll(@RequestBody final List<Task> tasks) {
        taskService.addAll(tasks);
        return tasks;
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") final String id) {
        taskService.removeById(id);
    }

    @Override
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        taskService.clear();
    }

}
